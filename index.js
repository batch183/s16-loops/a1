let num = parseInt(prompt("Provide a number:"));

console.log("The number you provided is " + num + ".");

for (num; num > 0; num--) {
	if (num <= 50) {
		console.log("The current value is less than or equal to 50. Terminating the loop.");
		break;
	}
	else if (num % 10 == 0) {
		console.log("The number is divisible by 10. Skipping the number.");
	}
	else if (num % 5 == 0) {
		console.log(num);
	}
	else {
		continue;
	}
}

let myString = "supercalifragilisticexpialidocious";
let myWord = '';

for (i=0; i < myString.length; i++ ) {
	if(myString[i].toLowerCase() == 'a' || myString[i].toLowerCase() == 'e'|| myString[i].toLowerCase() == 'i' || myString[i].toLowerCase() == 'o' || myString[i].toLowerCase() == 'u') {
		continue;
	}
	else {
		myWord += myString[i];

	}

}
console.log(myString);
console.log(myWord);